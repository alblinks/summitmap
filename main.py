import numpy as np
import os
import pandas as pd
import sys
from read_tourenbuch import read_fele_csv, read_yacq_csv


ascend_color_lookup = {
    "Vorstieg": "#00ff00",  # green
    "Nachstieg": "#ffff00",  # yellow
    "0": "#ff0000"  # red
    }


def get_kml_symbol(ascend: str) -> str:
    kml_symbol_lookup = {
        "Vorstieg": "http://maps.google.com/mapfiles/kml/paddle/grn-blank.png",
        "Nachstieg": "http://maps.google.com/mapfiles/kml/paddle/ylw-blank.png",
        "Toprope": "http://maps.google.com/mapfiles/kml/paddle/blu-blank.png",
        "0": "http://maps.google.com/mapfiles/kml/paddle/red-blank.png"
    }

    kml_icon_lookup = """<Style>
            <IconStyle>
              <Icon>
                {link}
              </Icon>
            </IconStyle>
          </Style>""".format(link=kml_symbol_lookup[str(ascend)])
    return kml_icon_lookup


def create_kml(df: pd.DataFrame, output_file: str) -> None:
    kml_template = '''<?xml version="1.0" encoding="UTF-8"?>
    <kml xmlns="http://earth.google.com/kml/2.0">
        <Document>
            <name>gipfelsammeln</name>
            {0}
        </Document>
    </kml>
    '''

    placemark_template = '''<Placemark>
        <name>{name}</name>
        <description></description>
        {style}
        <Point>
            <coordinates>{lon},{lat},0</coordinates>
        </Point>
    </Placemark>
    '''

    placemarks = ''
    for lat, lon, name, ascend in zip(df['lat'], df['lon'], df['name'], df['ascend']):
        placemarks += placemark_template.format(lat=lat, lon=lon, name=name, ascend=ascend_color_lookup[str(ascend)],
                                                style=get_kml_symbol(ascend))

    kml_data = kml_template.format(placemarks)

    with open("out/" + output_file + ".kml", 'w', encoding='utf-8') as f:
        f.write(kml_data)
        print("KML file written to /out/" + output_file + ".kml")


def create_gpx(df: pd.DataFrame, output_file: str) -> None:
    gpx_template = '''<?xml version="1.0" encoding="UTF-8"?>
        
<gpx version="1.1" creator="gipfelsammeln.py" xmlns="http://www.topografix.com/GPX/1/1" \
xmlns:osmand="https://osmand.net" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    <name>gipfelsammeln</name>
        {0}
    </gpx>'''

    point_template = '''<wpt lat="{lat}" lon="{lon}">
        <ele>0</ele>
        <name>{name}</name>
        <sym>waypoint dot</sym>
        <extensions>
         <osmand:icon>special_star</osmand:icon>
         <osmand:background>circle</osmand:background>
         <osmand:color>{ascend}</osmand:color>
        </extensions>
    </wpt>
    '''

    points = ''
    for lat, lon, name, ascend in zip(df['lat'], df['lon'], df['name'], df['ascend']):
        points += point_template.format(lat=lat, lon=lon, name=name, ascend=ascend_color_lookup[str(ascend)])
    gpx_data = gpx_template.format(points)

    with open("out/" + output_file + ".gpx", 'w', encoding='utf-8') as f:
        f.write(gpx_data)
        print("GPX file written to /out/" + output_file + ".gpx")



def main(*args):
    # if out folder does not exist
    # create out folder
    if not os.path.exists("/out"):
        os.makedirs("/out")

    if args[2] in ["fele"]:
        own = read_fele_csv(args[0])
    else:
        own = read_yacq_csv(args[0])
    all_summits = pd.read_csv('ressources/gipfel.csv', sep=',', header=0, encoding='utf-8')
    all_summits = all_summits.assign(ascend=0)
    # Find intersections in all_summits and add ascend
    own_list = own['name'].str.lower().to_numpy()
    all_summits_list = all_summits['name'].str.lower().to_numpy()
    overlap, own_idx, all_idx = np.intersect1d(own_list, all_summits_list, return_indices=True)
    all_summits.loc[all_idx, 'ascend'] = own.loc[own_idx, 'ascend'].values

    create_kml(all_summits, args[1])
    create_gpx(all_summits, args[1])


if __name__ == '__main__':
    input_file = "ressources/Tourenbuch.csv"
    output_file = "summits"
    source = "yacg"
    main(input_file, output_file, source)

