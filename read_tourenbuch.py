import pandas as pd
import re

df_columns = ['name', 'ascend', 'date']

ascend_remapping = {
    'Rotpunkt': 'Vorstieg',
    'Onsight': 'Vorstieg',
    'Solo': 'Vorstieg',
    'Alles Frei': 'Vorstieg',
    'Alles frei': 'Vorstieg',
    'Irgendwie hochgeschleudert': 'Vorstieg',
    'Wechselführung': 'Nachstieg',
    'Hinterhergehampelt': 'Nachstieg',
    'Gesackt': '0',
    'Will ich klettern': '0',
    'Unbekannt': '0'
}

def fix_name(s: str) -> str:
    pattern = r'([A-Z*])\1+$'
    return re.sub(pattern, '', s.replace('*', '').strip())

def read_fele_csv(fn: str):
    gipfel = pd.read_csv(fn, sep=';', encoding='latin-1', header=0)
    gipfel['Gipfel'] = gipfel['Gipfel'].apply(fix_name)
    return pd.DataFrame(gipfel[["Gipfel", "bestiegen", "Datum"]]).rename(
        columns={"Gipfel": "name", "bestiegen": "ascend", "Datum": "date"})

def read_yacq_csv(fn: str):
    gipfel = pd.read_csv(fn, sep=',', encoding='utf-8', header=0)
    return pd.DataFrame(gipfel[["rockFirstName", "style", "date"]]).rename(
        columns={"rockFirstName": "name", "style": "ascend"}).replace({"ascend": ascend_remapping})


if __name__ == '__main__':
    read_fele_csv('ressources/export.csv')
    read_yacq_csv('ressources/yacq.csv')
