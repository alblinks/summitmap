import pandas as pd
import re


def fix_name(s: str) -> str:
    pattern = r'([A-Z*])\1+$'
    return re.sub(pattern, '', s.replace('*', '').strip())


def retrieve_lat_lon(line: list[str]):
    name = ""
    lat = None
    for i, part in enumerate(line):
        if i == 0:
            nr = part
        elif part[0:2].isnumeric() and line[i+2] == "nördlicher":
            lat = float(part)
            assert 49 < lat < 52, f"Latitude {lat} not in valid range"
        elif part[0:2].isnumeric() and line[i+2] == "östlicher":
            lon = float(part)
            assert 12 < lon < 15, f"Longitude {lon} not in valid range"
        elif not lat:
            name += " " + part

    name = fix_name(name.strip())
    return nr, name, lat, lon


def read_txt(fn: str) -> (pd.DataFrame, list[str]):
    """Read txt file of Joes DB and returns Gipfelname, Gipfelnummer and lat/lon"""
    gipfel = []
    errors = []
    with open(fn, 'r', encoding="UTF-8") as f:
        for line in f:
            if line[0].isdigit():  # Assume Joe has build the RTF in a consistent way and avoid regex
                try:
                    gipfel.append(retrieve_lat_lon(line.split(' ')))
                except Exception as e:
                    errors.append(line)
    df = pd.DataFrame(gipfel, columns=["nr", "name", "lat", "lon"])
    return df, errors



if __name__ == '__main__':
    df, _ = read_txt('ressources/klefue.txt')
    df.to_csv('ressources/gipfel.csv', index=False, header=True, encoding='utf-8')
